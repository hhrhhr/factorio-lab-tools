local SettingLoader = {}

string.unpack = string.unpack or function(fmt, str)
    local res = 0
    if "<I2" == fmt then
        res = res + string.byte(str:sub(1, 1))
        res = res + string.byte(str:sub(2, 2)) * 2^8
    elseif "<i4" == fmt then
        res = res + string.byte(str:sub(1, 1))
        res = res + string.byte(str:sub(2, 2)) * 2^8
        res = res + string.byte(str:sub(3, 3)) * 2^16
        res = res + string.byte(str:sub(4, 4)) * 2^24
        if res > 2147483647 then
            res = res - 4294967296
        end
    elseif "<d" == fmt then
        res = res + string.byte(str:sub(1, 1))
        res = res + string.byte(str:sub(2, 2)) * 2^8
        res = res + string.byte(str:sub(3, 3)) * 2^16
        res = res + string.byte(str:sub(4, 4)) * 2^24
        local xh = res
        res = 0
        res = res + string.byte(str:sub(5, 5))
        res = res + string.byte(str:sub(6, 6)) * 2^8
        res = res + string.byte(str:sub(7, 7)) * 2^16
        res = res + string.byte(str:sub(8, 8)) * 2^24
        local ml = res
        local mh = bit32.extract(xh, 0, 20)
        local exp = bit32.extract(xh, 20, 11) - 1023
        local sign = bit32.extract(xh, 31, 1)
        if sign == 0 then sign = 1 else sign = -1 end
        local mul = 1
        res = 1
        for i = 19, 0, -1 do
            mul = mul * 0.5
            res = bit32.extract(mh, i) * mul + res
        end
        for i = 31, 0, -1 do
            mul = mul * 0.5
            res = bit32.extract(ml, i) * mul + res
        end
        local f = 0.0
        if exp ~= -1023 and ml ~= 0 and mh ~= 0 then
            f = sign * res * math.pow(2, exp)
        end
        res = f
    else
        assert(false)
    end
    return res
end


local function readAll(f, n)
    local result = ""
    while n > 0 do
        local s = f:read(n)
        n = n - #s
        result = result .. s
    end
    return result
end

function SettingLoader.readByte(f)
    return string.byte(readAll(f, 1), 1)
end

function SettingLoader.readBool(f)
    return SettingLoader.readByte(f) == 1
end

function SettingLoader.readUshort(f)
    return string.unpack("<I2", readAll(f, 2))
end

function SettingLoader.readInt(f)
    return string.unpack("<i4", readAll(f, 4))
end

function SettingLoader.readFloat(f)
    return string.unpack("<d", readAll(f, 8))
end

function SettingLoader.readString(f)
    if SettingLoader.readBool(f) then
        return ""
    end
    local length = SettingLoader.readByte(f)
    if length == 0xFF then
        length = SettingLoader.readInt(f)
    end
    return readAll(f, length)
end

function SettingLoader.readPropertyTree(f, depth)
    local treeType = SettingLoader.readByte(f)
    local anyType = readAll(f, 1)  -- discard
    if treeType == 0 then
        return {}
    elseif treeType == 1 then
        return SettingLoader.readBool(f)
    elseif treeType == 2 then
        return SettingLoader.readFloat(f)
    elseif treeType == 3 then
        return SettingLoader.readString(f)
    elseif treeType == 4 then
        local result = {}
        local length = SettingLoader.readInt(f)
        for x = 1, length do
            SettingLoader.readString(f)
            table.insert(result, SettingLoader.readPropertyTree(f))
        end
        return result
    elseif treeType == 5 then
        local result = {}
        local length = SettingLoader.readInt(f)
        for x = 1, length do
            local key = SettingLoader.readString(f)
            result[key] = SettingLoader.readPropertyTree(f, depth+1)
        end
        return result
    end
end

function SettingLoader.load(filename)
    local f = io.open(filename, "rb")
    if f == nil then
        return {
            ["startup"] = {},
            ["runtime-global"] = {},
            ["runtime-per-user"] = {}
        }
    end
    local version = {
        major = SettingLoader.readUshort(f),
        minor = SettingLoader.readUshort(f),
        patch = SettingLoader.readUshort(f),
        map = SettingLoader.readUshort(f)
    }
    if (version.major == 0 and version.minor >= 17) or version.major >= 1 then
        -- ignore extranous byte present since 0.17
        readAll(f, 1)
    end
    local settings = SettingLoader.readPropertyTree(f, 0)
    f:close()
    return settings
end

return SettingLoader
